import React from 'react'
import Link from 'next/link'
import { AiOutlineShopping } from 'react-icons/ai'

import { Cart } from './'
import { useStateContext } from '../context/StateContext'

const Navbar = () => {
  const {showCart, setShowCart, totalQuantities} = useStateContext()
  return (
    <div className="navbar-container">
      <h3>
        <Link href="/">Acoustics <span>Lounge</span></Link>
      </h3>
      <div className="cart-icon-container">
        <button type="button" className="cart-icon" onClick={() => setShowCart(true)}>
          <AiOutlineShopping/>
          <span className="cart-item-qty">{totalQuantities}</span>
        </button>
      </div>

      {showCart &&<Cart/>}
    </div>
  )
}

export default Navbar